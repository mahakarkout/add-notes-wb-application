"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// jest.test.ts
const supertest_1 = __importDefault(require("supertest"));
const app_1 = __importDefault(require("../src/app"));
describe("Backend API Tests", () => {
    it("should return an error if title or content is missing when updating a note", () => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield (0, supertest_1.default)(app_1.default).put("/notes/1").send({ title: "", content: "" });
        expect(response.status).toBe(400);
        expect(response.text).toBe("title and content fields required");
    }));
    it("should return an error if ID is not a valid number when updating a note", () => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield (0, supertest_1.default)(app_1.default).put("/notes/abc").send({ title: "Title", content: "Content" });
        expect(response.status).toBe(400);
        expect(response.text).toBe("ID must be a valid number");
    }));
    it("should return an error if ID is missing or not a valid number when deleting a note", () => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield (0, supertest_1.default)(app_1.default).delete("/notes/abc");
        expect(response.status).toBe(400);
        expect(response.text).toBe("ID field required");
    }));
    // Optionally, add more tests to cover other routes and scenarios
    /*
    it("should get all notes", async () => {
      const response = await request(app).get("/notes");
      expect(response.status).toBe(200);
      expect(Array.isArray(response.body)).toBe(true);
      // Add more assertions as needed
    });
  
    it("should create a new note", async () => {
      const response = await request(app)
        .post("/notes")
        .send({ title: "Test Note", content: "This is a test note" });
      expect(response.status).toBe(200);
      expect(response.body.title).toBe("Test Note");
      expect(response.body.content).toBe("This is a test note");
    });
    */
});

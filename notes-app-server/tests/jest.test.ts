// jest.test.ts
import request from "supertest";
import app from "../src/app";


describe("Backend API Tests", () => {

  it("should return an error if title or content is missing when updating a note", async () => {
    const response = await request(app).put("/notes/1").send({ title: "", content: "" });
    expect(response.status).toBe(400);
    expect(response.text).toBe("title and content fields required");
  });

  it("should return an error if ID is not a valid number when updating a note", async () => {
    const response = await request(app).put("/notes/abc").send({ title: "Title", content: "Content" });
    expect(response.status).toBe(400);
    expect(response.text).toBe("ID must be a valid number");
  });

  it("should return an error if ID is missing or not a valid number when deleting a note", async () => {
    const response = await request(app).delete("/notes/abc");
    expect(response.status).toBe(400);
    expect(response.text).toBe("ID field required");
  });



  // it("should get all notes", async () => {
  //   const response = await request(app).get("/notes");
  //   expect(response.status).toBe(200);
  //   expect(Array.isArray(response.body)).toBe(true);
  //   // Add more assertions as needed
  // });

  // it("should create a new note", async () => {
  //   const response = await request(app)
  //     .post("/notes")
  //     .send({ title: "Test Note", content: "This is a test note" });
  //   expect(response.status).toBe(200);
  //   expect(response.body.title).toBe("Test Note");
  //   expect(response.body.content).toBe("This is a test note");
  // });
  
});

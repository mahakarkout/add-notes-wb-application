(async () => {
const { expect } = await require('chai');
const request = await require('supertest');
const app = await require('../notes-app/src/App.tsx'); // Assuming your Express app is exported from app.js


describe('GET /notes', () => {
    it('should return an array of notes', async () => {
      const res = await request(app).get('/notes');
      expect(res.status).to.equal(200);
      expect(res.body).to.be.an('array');
    });
  });

  describe('POST /notes', () => {
    it('should create a new note', async () => {
      const newNote = { title: 'Test Note', content: 'This is a test note' };
      const res = await request(app)
        .post('/notes')
        .send(newNote);
      expect(res.status).to.equal(200);
      expect(res.body).to.have.property('id');
      expect(res.body.title).to.equal(newNote.title);
      expect(res.body.content).to.equal(newNote.content);
    });

    it('should return 400 if title or content is missing', async () => {
      const res = await request(app)
        .post('/notes')
        .send({ title: 'Missing Content' });
      expect(res.status).to.equal(400);
    });
  });
  
  // Run the tests using Mocha
  run();
})();
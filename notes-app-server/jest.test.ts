import request from "supertest";
import express from "express";
import { PrismaClient } from "@prisma/client";
import cors from "cors";

const prisma = new PrismaClient();
const app = express();

app.use(express.json());
app.use(cors());

app.get("/notes", async (req, res) => {
  const notes = await prisma.note.findMany();
  res.json(notes);
});

app.post("/notes", async (req, res) => {
  const { title, content } = req.body;

  if (!title || !content) {
    return res.status(400).send("title and content fields required");
  }

  try {
    const note = await prisma.note.create({
      data: { title, content },
    });
    res.json(note);
  } catch (error) {
    res.status(500).send("Oops, something went wrong");
  }
});
app.put("/notes/:id", async (req, res) => {
  const { title, content } = req.body;
  const id = parseInt(req.params.id);

  if (!title || !content) {
    return res.status(400).send("title and content fields required");
  }

  if (!id || isNaN(id)) {
    return res.status(400).send("ID must be a valid number");
  }

  try {
    const updatedNote = await prisma.note.update({
      where: { id },
      data: { title, content },
    });
    res.json(updatedNote);
  } catch (error) {
    res.status(500).send("Oops, something went wrong");
  }
});

app.delete("/notes/:id", async (req, res) => {
  const id = parseInt(req.params.id);

  if (!id || isNaN(id)) {
    return res.status(400).send("ID field required");
  }

  try {
    await prisma.note.delete({
      where: { id },
    });
    res.status(204).send();
  } catch (error) {
    res.status(500).send("Oops, something went wrong");
  }
  
});

// import { Express, Request, Response } from 'express';
// import { Server } from 'http';

import { Application } from 'express';
import http, { Server } from 'http';
// import request from 'supertest';
// import { app } from './app';

let server: Server;
let appInstance: Application;

beforeEach(() => {
  const PORT = 3000;
  appInstance = express();
  server = appInstance.listen(PORT);
});

afterEach(() => {
  if (server) {
    server.close();
  }
});
describe("Backend API Tests", () => {

  // it("should get notes", async () => {
  //   const response = await request(app).get("/notes");
  //   expect(response.status).toBe(200);

  //   // Check if the response body is an array
  //   expect(Array.isArray(response.body)).toBe(true);

  //   // Check if each item in the array is an object with specific keys
  //   response.body.forEach((note: { content: string, id: number, title: string }) => {
  //     expect(note).toEqual(
  //       expect.objectContaining({
  //         content: expect.any(String),
  //         id: expect.any(Number),
  //         title: expect.any(String),
  //         // Add more key expectations as needed
  //       })
  //     );
  //   });
  // });

  // it("should create a new note", async () => {
  //   const response = await request(app)
  //     .post("/notes")
  //     .send({ title: "Test Note", content: "This is a test note" });
  //     // Log the request being sent
  // console.log("Request sent to create a new note:", response.request);

  // // Log the response received
  // console.log("Response received after creating a new note:", response);
  //   expect(response.status).toBe(200);
  //   expect(response.body.title).toBe("Test Note");
  //   expect(response.body.content).toBe("This is a test note");
  // });

  it("should return an error if title or content is missing when updating a note", async () => {
    const response = await request(app).put("/notes/1").send({ title: "", content: "" });
    expect(response.status).toBe(400);
    expect(response.text).toBe("title and content fields required");
  });

  it("should return an error if ID is not a valid number when updating a note", async () => {
    const response = await request(app).put("/notes/abc").send({ title: "Title", content: "Content" });
    expect(response.status).toBe(400);
    expect(response.text).toBe("ID must be a valid number");
  });

  it("should return an error if ID is missing or not a valid number when deleting a note", async () => {
    const response = await request(app).delete("/notes/abc");
    expect(response.status).toBe(400);
    expect(response.text).toBe("ID field required");
  });
});

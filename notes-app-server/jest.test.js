"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const supertest_1 = __importDefault(require("supertest"));
const express_1 = __importDefault(require("express"));
const client_1 = require("@prisma/client");
const cors_1 = __importDefault(require("cors"));
const prisma = new client_1.PrismaClient();
const app = (0, express_1.default)();
app.use(express_1.default.json());
app.use((0, cors_1.default)());
app.get("/notes", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const notes = yield prisma.note.findMany();
    res.json(notes);
}));
app.post("/notes", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { title, content } = req.body;
    if (!title || !content) {
        return res.status(400).send("title and content fields required");
    }
    try {
        const note = yield prisma.note.create({
            data: { title, content },
        });
        res.json(note);
    }
    catch (error) {
        res.status(500).send("Oops, something went wrong");
    }
}));
app.put("/notes/:id", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { title, content } = req.body;
    const id = parseInt(req.params.id);
    if (!title || !content) {
        return res.status(400).send("title and content fields required");
    }
    if (!id || isNaN(id)) {
        return res.status(400).send("ID must be a valid number");
    }
    try {
        const updatedNote = yield prisma.note.update({
            where: { id },
            data: { title, content },
        });
        res.json(updatedNote);
    }
    catch (error) {
        res.status(500).send("Oops, something went wrong");
    }
}));
app.delete("/notes/:id", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const id = parseInt(req.params.id);
    if (!id || isNaN(id)) {
        return res.status(400).send("ID field required");
    }
    try {
        yield prisma.note.delete({
            where: { id },
        });
        res.status(204).send();
    }
    catch (error) {
        res.status(500).send("Oops, something went wrong");
    }
}));
// import request from 'supertest';
// import { app } from './app';
let server;
let appInstance;
beforeEach(() => {
    const PORT = 3000;
    appInstance = (0, express_1.default)();
    server = appInstance.listen(PORT);
});
afterEach(() => {
    if (server) {
        server.close();
    }
});
describe("Backend API Tests", () => {
    // it("should get notes", async () => {
    //   const response = await request(app).get("/notes");
    //   expect(response.status).toBe(200);
    //   // Check if the response body is an array
    //   expect(Array.isArray(response.body)).toBe(true);
    //   // Check if each item in the array is an object with specific keys
    //   response.body.forEach((note: { content: string, id: number, title: string }) => {
    //     expect(note).toEqual(
    //       expect.objectContaining({
    //         content: expect.any(String),
    //         id: expect.any(Number),
    //         title: expect.any(String),
    //         // Add more key expectations as needed
    //       })
    //     );
    //   });
    // });
    // it("should create a new note", async () => {
    //   const response = await request(app)
    //     .post("/notes")
    //     .send({ title: "Test Note", content: "This is a test note" });
    //     // Log the request being sent
    // console.log("Request sent to create a new note:", response.request);
    // // Log the response received
    // console.log("Response received after creating a new note:", response);
    //   expect(response.status).toBe(200);
    //   expect(response.body.title).toBe("Test Note");
    //   expect(response.body.content).toBe("This is a test note");
    // });
    it("should return an error if title or content is missing when updating a note", () => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield (0, supertest_1.default)(app).put("/notes/1").send({ title: "", content: "" });
        expect(response.status).toBe(400);
        expect(response.text).toBe("title and content fields required");
    }));
    it("should return an error if ID is not a valid number when updating a note", () => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield (0, supertest_1.default)(app).put("/notes/abc").send({ title: "Title", content: "Content" });
        expect(response.status).toBe(400);
        expect(response.text).toBe("ID must be a valid number");
    }));
    it("should return an error if ID is missing or not a valid number when deleting a note", () => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield (0, supertest_1.default)(app).delete("/notes/abc");
        expect(response.status).toBe(400);
        expect(response.text).toBe("ID field required");
    }));
});

// jest.config.js
module.exports = {
  preset: 'ts-jest', // Enables TypeScript support
  testEnvironment: 'node',
  collectCoverage: true, // Enables coverage collection
  coverageDirectory: 'coverage', // Output directory for coverage reports
  coverageReporters: ['lcov', 'text'], // Formats for coverage reports
  collectCoverageFrom: [
    'app.ts', // Include app.ts for coverage
    'src/**/*.{ts,tsx}', // Adjust based on your source files' location
    '!src/**/*.d.ts', // Exclude TypeScript declaration files
    // '!src/index.ts', // Excluded by default
  ],
  coveragePathIgnorePatterns: [
    '/node_modules/', // Exclude dependencies
    '/tests/', // Exclude test directories if necessary
  ],
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'], // Recognized file extensions
  testMatch: ['**/?(*.)+(spec|test).ts?(x)'], // Pattern for test files
};

#!/bin/bash
set -e  # Exit immediately if a command exits with a non-zero status.

# Run Prisma migrations
echo "Running database migrations..."
npx prisma migrate deploy --schema=./prisma/schema.prisma

# Start the application
echo "Starting the application..."
npm start
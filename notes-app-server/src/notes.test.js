"use strict";
   
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// import { render } from '@testing-library/react';
const App_1 = __importDefault(require("../../notes-app/src/App.tsx"));
const chai_1 = __importDefault(require("chai"));
const chai_http_1 = __importDefault(require("chai-http"));
// import app from "./app"; // Import your Express app
const expect = chai_1.default.expect;
chai_1.default.use(chai_http_1.default);
describe("API Tests", () => {
    it("should get all notes", (done) => {
        chai_1.default
            .request(App_1.default)
            .get("/notes")
            .end((err, res) => {
            expect(res).to.have.status(200);
            expect(res.body).to.be.an("array");
            done();
        });
    });
    it("should create a new note", (done) => {
        const newNote = { title: "Test Title", content: "Test Content" };
        chai_1.default
            .request(App_1.default)
            .post("/notes")
            .send(newNote)
            .end((err, res) => {
            expect(res).to.have.status(200);
            expect(res.body).to.deep.include(newNote);
            done();
        });
    });
    it("should return 400 if title or content is missing", (done) => {
        const invalidNote = { title: "Test Title" }; // Missing content field
        chai_1.default
            .request(App_1.default)
            .post("/notes")
            .send(invalidNote)
            .end((err, res) => {
            expect(res).to.have.status(400);
            expect(res.text).to.equal("title and content fields required");
            done();
        });
    });
});


// import { useState as useStateMock } from 'react';
// import App from '../../notes-app/src/App'; // Assuming App.js is the file where your Express app is defined
// jest.mock('react', () => ({
//   ...jest.requireActual('react'),
//   useState: jest.fn(),
// }));
// describe("GET /notes", () => {
//   it("should return a list of notes", async () => {
//     const response = await request(App).get("/notes");
//     expect(response.status).toBe(200);
//     expect(Array.isArray(response.body)).toBe(true); // Check if response body is an array
//     // Add additional expectations based on the structure of the response data
//     // For example, if the response contains an array of notes with a specific property like 'title':
//     expect(response.body.length).toBeGreaterThan(0); // Check if the array is not empty
//     expect(response.body[0]).toHaveProperty('title'); // Check if the first item has a 'title' property
//   });
// });
// describe("POST /notes", () => {
//   it("should create a new note", async () => {
//     const newNote = { title: "Test Note", content: "Test Content" };
//     const response = await request(App).post("/notes").send(newNote);
//     expect(response.status).toBe(200);
//     expect(response.body).toMatchObject(newNote);
//   });
//   it("should return an error if title or content is missing", async () => {
//     const response = await request(App).post("/notes").send({});
//     expect(response.status).toBe(400);
//   });
// });
// // Add similar test cases for PUT and DELETE routes
// // afterAll(() => {
// //   app.close(); // Close the server after all tests are done
// // });
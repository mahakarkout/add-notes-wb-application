// "use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(require("react"));
require("./App.css");
const App = () => {
    const [title, setTitle] = (0, react_1.useState)("");
    const [content, setContent] = (0, react_1.useState)("");
    const [notes, setNotes] = (0, react_1.useState)([]);
    (0, react_1.useEffect)(() => {
        const fetchNotes = () => __awaiter(void 0, void 0, void 0, function* () {
            try {
                const response = yield fetch("http://localhost:5000/notes");
                const notes = yield response.json();
                setNotes(notes);
            }
            catch (e) {
                console.log(e);
            }
        });
        fetchNotes();
    }, []);
    const [selectedNote, setSelectedNote] = (0, react_1.useState)(null);
    const handleAddNote = (event) => __awaiter(void 0, void 0, void 0, function* () {
        event.preventDefault();
        try {
            const response = yield fetch("http://localhost:5000/notes", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    title,
                    content,
                }),
            });
            const newNote = yield response.json();
            setNotes([newNote, ...notes]);
            setTitle("");
            setContent("");
        }
        catch (e) {
            console.log(e);
        }
    });
    const handleNoteClick = (note) => {
        setSelectedNote(note);
        setTitle(note.title);
        setContent(note.content);
    };
    const handleUpdateNote = (event) => __awaiter(void 0, void 0, void 0, function* () {
        event.preventDefault();
        if (!selectedNote) {
            return;
        }
        const updatedNote = {
            id: selectedNote.id,
            title: title,
            content: content,
        };
        try {
            const response = yield fetch(`http://localhost:5000/notes/${selectedNote.id}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(updatedNote),
            });
            if (response.ok) {
                const updatedNotesList = notes.map((note) => (note.id === selectedNote.id ? updatedNote : note));
                setNotes(updatedNotesList);
                setTitle("");
                setContent("");
                setSelectedNote(null);
            }
            else {
                console.error("Failed to update note");
            }
        }
        catch (error) {
            console.error(error);
        }
    });
    const handleCancel = () => {
        setTitle("");
        setContent("");
        setSelectedNote(null);
    };
    const deleteNote = (event, noteId) => __awaiter(void 0, void 0, void 0, function* () {
        event.stopPropagation();
        try {
            yield fetch(`http://localhost:5000/notes/${noteId}`, {
                method: "DELETE",
            });
            const updatedNotes = notes.filter((note) => note.id !== noteId);
            setNotes(updatedNotes);
        }
        catch (e) {
            console.log(e);
        }
    });
    return (react_1.default.createElement("div", { className: "app-container" },
        react_1.default.createElement("div", null,
            react_1.default.createElement("h1", null, "My Notes App"),
            react_1.default.createElement("p", null, "Learn React")),
        react_1.default.createElement("form", { className: "note-form", onSubmit: (event) => (selectedNote ? handleUpdateNote(event) : handleAddNote(event)) },
            react_1.default.createElement("input", { value: title, onChange: (event) => setTitle(event.target.value), placeholder: "Title", required: true }),
            react_1.default.createElement("textarea", { value: content, onChange: (event) => setContent(event.target.value), placeholder: "Content", rows: 10, required: true }),
            selectedNote ? (react_1.default.createElement("div", { className: "edit-buttons" },
                react_1.default.createElement("button", { type: "submit" }, "Save"),
                react_1.default.createElement("button", { onClick: handleCancel }, "Cancel"))) : (react_1.default.createElement("button", { type: "submit" }, "Add Note"))),
        react_1.default.createElement("div", { className: "notes-grid" }, notes.map((note) => (react_1.default.createElement("div", { key: note.id, className: "note-item", onClick: () => handleNoteClick(note) },
            react_1.default.createElement("div", { className: "notes-header" },
                react_1.default.createElement("button", { onClick: (event) => deleteNote(event, note.id) }, "x")),
            react_1.default.createElement("h2", null, note.title),
            react_1.default.createElement("p", null, note.content)))))));
};
exports.default = App;
